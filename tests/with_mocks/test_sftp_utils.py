"""Mock testing of sftp routines."""

# ruff: noqa: S101, ARG001, ARG002, ANN002, ANN003

import os.path
import tempfile
from subprocess import CalledProcessError
from unittest.mock import ANY, MagicMock, Mock, call, patch

from sftp_utils import sftp_get, sftp_put, sftp_waitget


@patch("subprocess.run")
def test_sftp_get(mock_subprocess_run: Mock) -> None:
    """Mock sftp interaction to test sftp_get."""
    mock_result = MagicMock()
    mock_result.returncode = 0
    mock_subprocess_run.return_value = mock_result

    with tempfile.NamedTemporaryFile("w", encoding="utf8") as tmp:
        # Write some content to a local file so that sftp_get can get its file
        # size as if it was the file downloaded.
        tmp.write("We pretend that sftp_get downloaded a file to this tempfile.")
        tmp.flush()

        local_file_size = sftp_get(
            "mock.example.com",
            "user",
            "pass",
            "remote/test_file.csv",
            tmp.name,
        )
        assert local_file_size == os.path.getsize(tmp.name)

    mock_subprocess_run.assert_called_once_with(
        ["lftp", "-f", ANY],
        stderr=-1,
        check=True,
    )


@patch("subprocess.run")
def test_sftp_put(mock_subprocess_run: Mock) -> None:
    """Mock sftp interaction to test sftp_put."""
    mock_result = MagicMock()
    mock_result.returncode = 0
    mock_subprocess_run.return_value = mock_result

    with tempfile.NamedTemporaryFile("w", encoding="utf8") as tmp:
        # Write a local file for sftp_put to mock upload.
        tmp.write("A file to upload.")
        tmp.flush()
        sftp_put(
            "mock.example.com",
            "user",
            "pass",
            tmp.name,
            "remote/test_file.csv",
        )

    mock_subprocess_run.assert_called_once_with(
        ["lftp", "-f", ANY],
        stderr=-1,
        check=True,
    )


class SuccessOnAttempt:
    """subprocess.run side effect so that it executes successfully after three failures.

    This simulates the case where waitget case where the remote file isn't found
    until the forth attempt.
    """

    def __init__(self, success_on_attempt_n: int) -> None:
        """Init."""
        self.call_count = 0
        self.success_on_attempt_n = success_on_attempt_n

    def __call__(self, *args, **kwargs) -> None:
        """Callable for side effect."""
        self.call_count += 1
        if self.call_count < self.success_on_attempt_n:
            raise CalledProcessError(1, "lftp", "", "download failed")


@patch("subprocess.run")
@patch("time.sleep", return_value=None)  # mock to avoid actual delay.
def test_sftp_waitget(mock_sleep: Mock, mock_subprocess_run: Mock) -> None:
    """Mock sftp interaction to test sftp_waitget."""
    # There will be five total lftp download attempts. Attempt 4 is the first
    # successful attempt. Attempt 5 is the extra waitget attempt to verify the data is
    # stable.
    total_download_attempts = 5
    mock_subprocess_run.side_effect = SuccessOnAttempt(total_download_attempts - 1)

    with tempfile.NamedTemporaryFile("w", encoding="utf8") as tmp:
        sftp_waitget(
            "mock.example.com",
            "user",
            "pass",
            "remote/test_file.csv",
            tmp.name,
            pre_delete_local=False,
        )

        assert mock_subprocess_run.call_count == total_download_attempts

        expected_calls = [
            call(
                ["lftp", "-f", ANY],
                stderr=-1,
                check=True,
            )
        ] * total_download_attempts

        mock_subprocess_run.assert_has_calls(expected_calls)
