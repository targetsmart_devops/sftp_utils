# sftp_utils

Python helper routines for transferring data to/from vendor SFTP hosts that use
user/password authentication.


## Installation

``` sh
pip install git+https://git@bitbucket.org/targetsmart_devops/sftp_utils
```

## Requirements

Due to performance limitations of paramiko, these functions require the `lftp`
command to be installed.

`yum install lftp`

## Usage

### Uploading to a vendor

``` python
from sftp_utils import sftp_put

sftp_put('sedt.infutor.com', 'way78923', '********', '/files/local.csv', 'remote.csv')

```


### Downloading from a vendor

``` python
from sftp_utils import sftp_get

sftp_get('sedt.infutor.com', 'way78923', '********', '/Finished/remote.csv', 'local.csv') 

# OR using `remote_dir`:

sftp_get('sedt.infutor.com', 'way78923', '********', 'remote.csv', 'local.csv', remote_dir='Finished')


```

### Polling for a remote file to become available

``` python
from sftp_utils import sftp_waitget

# Blocks until the file is downloaded or times out
sftp_waitget('sedt.infutor.com', 'way78923', '********', '/Finished/remote.csv', 'local.csv',
             wait=60*2, timeout=60 * 60 * 24)


```
