"""SFTP routines.

Helper routines for transferring data to/from vendor SFTP hosts that use
user/password authentication.
"""

from __future__ import annotations

import datetime
import logging
import os.path
import subprocess
import tempfile
import threading
import time
from typing import TYPE_CHECKING

from slack2 import Slack

if TYPE_CHECKING:
    from slack2.parent_message import ParentMessage


logger = logging.getLogger(__name__)


class SftpUtilsError(Exception):
    """
    Indicates SFTP request failed.
    """


class LFTPWrapper:
    """Wrapper for subprocess interaction with lftp command."""

    _script_base: str | None
    lftp_version: str

    def __init__(self) -> None:
        """Init.

        Two subprocesses are called. One to get the lftp version. One to for the
        base set commands that are used as a basis for sftp_get and sftp_put
        lftp commands.
        """
        self._script_base = None
        self.lftp_version = self.get_lftp_version()

        # runs subprocess and caches to _script_base.
        self.get_lftp_script_base()

    def has_sftp_auto_confirm_option(self) -> bool:
        """Check if the lftp version supports the "sftp:auto-confirm" option."""
        proc = subprocess.run(
            ["lftp", "-c", "set -a"],  # noqa: S603, S607
            capture_output=True,
            check=True,
            encoding="utf-8",
            errors="ignore",
        )
        return "sftp:auto-confirm" in proc.stdout

    def get_lftp_version(self) -> str:
        """Return lftp version info."""
        proc = subprocess.run(
            ["lftp", "--version"],  # noqa: S603, S607
            capture_output=True,
            check=True,
            encoding="utf-8",
            errors="ignore",
        )
        return proc.stdout.split("\n")[0]

    def log_lftp_version(self) -> None:
        """Log lftp version info."""
        logger.info(self.lftp_version)

    def get_lftp_script_base(self) -> str:
        """Return the base settings for both get and put lftp commands.

        Newer versions of lftp support the sftp:auto-confirm option which must be
        enabled when available.
        """
        if self._script_base:
            return self._script_base

        if self.has_sftp_auto_confirm_option():
            self._script_base = """\
set xfer:clobber on;
set sftp:auto-confirm yes;
"""
        else:
            self._script_base = """\
set xfer:clobber on;
"""
        return self._script_base

    def run_lftp(self, script: str) -> None:
        """Execute lftp with file command script.

        Args:
            script: lftp script to execute.
        """
        # lftp executes plain text commands that include password. To make this
        # idiom more secure, we use tempfile.
        with tempfile.NamedTemporaryFile(
            mode="w+", encoding="utf8", newline="\n"
        ) as tmp:
            tmp.write(script)
            tmp.flush()
            try:
                # [S603]: Check for untrusted input.
                #
                #   I think we can ignore because tmp.name won't produce unsanitized
                #   text.
                #
                # [S607]: Starting a process with partial executable path.
                #
                #   I'm not confident that lftp will always be installed to /usr/bin
                #   for applications using this library.
                subprocess.run(
                    [  # noqa: S603, S607
                        "lftp",
                        "-f",
                        tmp.name,
                    ],
                    stderr=subprocess.PIPE,
                    check=True,
                )

            except subprocess.CalledProcessError as e:
                errmsg = f"lftp sftp command failed. Error: {e.stderr}. Return code: {e.returncode}."
                raise SftpUtilsError(errmsg) from e


LFTP = LFTPWrapper()


def sftp_get(
    host: str,
    user: str,
    password: str,
    remote_file: str,
    local_file: str,
    remote_dir: str | None = None,
    port: int = 22,
) -> int:
    """Download remote file.

    Args:
        host: SFTP hostname
        user: SFTP hostname
        password: SFTP password
        remote_file: Remote file name that will be stored. Can be full path or basename.
        local_file: Local filesystem file path that will be uploaded
        remote_dir: If remote_file is a basename, you may optionally provide the remote_dir value here.
        port: SFTP port

    Returns:
        Size in bytes of the downloaded file.
    """
    if remote_dir:
        remote_file = remote_dir.rstrip("/") + "/" + remote_file.lstrip("/")

    logger.info(
        f"SFTP download: {remote_file} -> {local_file}. Source: {user}@{host}:{port}."
    )
    LFTP.log_lftp_version()

    script = f"""\
{LFTP.get_lftp_script_base()}

open sftp://{user}:{password}@{host}:{port};

get '{remote_file}' -o '{local_file}';

exit;
"""
    LFTP.run_lftp(script)

    local_size = os.path.getsize(local_file)
    logger.info(f"Download complete. Local file size: {local_size}.")
    return local_size


def sftp_put(
    host: str,
    user: str,
    password: str,
    local_file: str,
    remote_file: str,
    remote_dir: str | None = None,
    port: int = 22,
) -> None:
    """
    Upload file to SFTP host.

    Args:
        host: SFTP hostname
        user: SFTP username
        password: SFTP password
        local_file: Local filesystem file path that will be uploaded
        remote_file: Remote file name that will be stored. Can be full path or basename.
        remote_dir: If remote_file is a basename, you may optionally provide the remote_dir value here.
        port: SFTP port
    """
    if local_file is None:
        errmsg = "Missing local_file"
        raise ValueError(errmsg)

    if remote_file is None:
        remote_file = os.path.basename(local_file)

    if remote_dir:
        remote_file = remote_dir.rstrip("/") + "/" + remote_file.lstrip("/")

    logger.info(
        f"SFTP upload: {local_file} -> {remote_file}. Source: {user}@{host}:{port}."
    )
    LFTP.log_lftp_version()

    script = f"""\
{LFTP.get_lftp_script_base()}

open sftp://{user}:{password}@{host}:{port};

put '{local_file}' -o '{remote_file}';

exit;
"""
    LFTP.run_lftp(script)


class SlackMsgState:
    """Allow background thread to pass back the parent msg for replies."""

    lock: threading.Lock
    _parent_msg: None | ParentMessage

    def __init__(self) -> None:
        """Init."""
        self.lock = threading.Lock()
        self._parent_msg = None

    def set_parent_msg(self, parent: ParentMessage) -> None:
        """Set the slack parent message to support threaded notifications."""
        with self.lock:
            self._parent_msg = parent

    def get_parent_msg(self) -> ParentMessage | None:
        """Get the slack parent message to support threaded notifications."""
        with self.lock:
            return self._parent_msg


def sftp_waitget_slack_notice(
    host: str,
    port: int,
    user: str,
    remote_file: str,
    slack_channel: str,
    slack_channel_interval: float,
    slack_msg_extra: str,
    slack_msg_state: SlackMsgState,
) -> None:
    """
    For background thread to send Slack notifications.
    """
    parent_msg = None
    wait = 0.0
    while True:
        time.sleep(slack_channel_interval)
        wait += slack_channel_interval
        logger.info(f"Sending Slack alert to #{slack_channel}.")
        delta = datetime.timedelta(seconds=wait)
        msg = f"*sftp_utils* notice\n`sftp_waitget` has been waiting for {delta} for `sftp://{user}@{host}:{port}/{remote_file}` to become available.\n\nExtra info:\n{slack_msg_extra}"

        if parent_msg:
            Slack.send(msg, channel=slack_channel, parent_message=parent_msg)
        else:
            parent_msg = Slack.send(msg, channel=slack_channel)
            if parent_msg:
                slack_msg_state.set_parent_msg(parent_msg)


def sftp_waitget(
    host: str,
    user: str,
    password: str,
    remote_file: str,
    local_file: str,
    remote_dir: str | None = None,
    port: int = 22,
    wait: int = 60 * 5,
    timeout: int = 60 * 60 * 24 * 4,
    slack_channel: str | None = None,
    slack_channel_interval: int = 60 * 60 * 3,
    slack_msg_extra: str | None = None,
    *,
    confirm_download: bool = True,
    pre_delete_local: bool = True,
) -> None:
    """Poll until remote file is successfully downloaded.

    A commonly encountered integration pattern in the data vendor industry is to
    poll for a remote file to become available. This routine will wait up to
    `timeout` seconds for `remote_file` to exist. If `confirm_download` is true,
    a second download will be done to verify stable file size. The routine will
    sleep `wait` seconds between each poll attempt.

    If `slack_channel` is used then a message will be sent every
    `slack_channel_interval` seconds to notify that the process is still waiting
    for results. Use `slack_msg_extra` to optionally provide addition info to
    include in the Slack notice.

    Args:
        host: SFTP hostname
        user: SFTP username
        password: SFTP password
        remote_file: Remote file name that will be stored. Can be full path or basename.
        local_file: Local file path that will be written.
        remote_dir: If remote_file is a basename, you may optionally provide the remote_dir value here.
        port: SFTP port.
        wait: Time to wait between each attempt (seconds).
        timeout: Maximum overall time before giving up (seconds).
        slack_channel: Optional Slack channel for alerts.
        slack_channel_interval: Alert notification interval (seconds).
        slack_msg_extra: Optional str value to include in notifications.
        confirm_download: If true, download file a second time to confirm data is stable.
        pre_delete_local: If true, delete local_file before SFTP operations.
    """
    if pre_delete_local and os.path.exists(local_file):
        os.remove(local_file)

    if remote_file is None:
        errmsg = "Missing remote_file. Cannot GET."
        raise SftpUtilsError(errmsg)

    if local_file is None:
        logger.info("local_file is None. Using remote_file for local_file.")
        local_file = remote_file

    slept = attempt = 0
    current_fsize = prev_fsize = slack_msg_state = None

    if slack_channel:
        logger.info(
            f"Alerts will be sent to the #{slack_channel} Slack channel every {slack_channel_interval} seconds."
        )
        slack_msg_state = SlackMsgState()
        threading.Thread(
            target=sftp_waitget_slack_notice,
            args=(
                host,
                port,
                user,
                remote_file,
                slack_channel,
                slack_channel_interval,
                slack_msg_extra,
                slack_msg_state,
            ),
            daemon=True,
        ).start()

    while True:
        attempt += 1
        logger.info(f"This is download attempt {attempt}.")

        # If total wait is greater than the timeout, stop
        # trying.  Note that timeout isn't exact.
        slept += wait

        if slept >= timeout:
            errmsg = "Waited too long. Giving up."
            raise SftpUtilsError(errmsg)

        current_fsize = None
        try:
            current_fsize = sftp_get(
                host,
                user,
                password,
                remote_file,
                local_file,
                remote_dir,
                port,
            )
        except SftpUtilsError as e:
            logger.info(f"This attempt failed with error '{e}'. Will continue to poll.")

        if current_fsize is not None and os.path.isfile(local_file):
            logger.info(
                f"Download successful. Size: {current_fsize}. Attempt: {attempt}."
            )

            logger.info(f"Current Size: {current_fsize}. Prev Size: {prev_fsize}.")

            if not confirm_download or current_fsize == prev_fsize:
                msg = f"SFTP Download of `{remote_file}` to `{local_file}` completed successfully."
                logger.info(msg)
                if slack_channel and slept > slack_channel_interval and slack_msg_state:
                    Slack.send(
                        f"🎉 {msg}",
                        channel=slack_channel,
                        parent_message=slack_msg_state.get_parent_msg(),
                    )
                return

            logger.info("Downloading file once again to confirm file size.")

        prev_fsize = current_fsize
        logger.info(f"Sleeping for {wait} seconds before next download attempt.")
        time.sleep(wait)
