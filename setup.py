"""Package setup."""

from setuptools import setup

setup(
    name="sftp_utils",
    version="0.2.0",
    description="Helper routines for transferring data to/from vendor SFTP hosts that use user/password authentication.",
    author="TargetSmart",
    author_email="devops@targetsmart.com",
    license="Copyright TargetSmart. All rights reserved.",
    url="https://bitbucket.org/targetsmart_devops/sftp_utils.git",
    download_url="https://bitbucket.org/targetsmart_devops/sftp_utils.git",
    test_suite="tests",
    py_modules=["sftp_utils"],
    install_requires=[
        "slack2 @ git+https://git@bitbucket.org/targetsmart_devops/slack2@main#egg=slack2"
    ],
)
